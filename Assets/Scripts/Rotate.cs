﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    public float rotationRate = 100f;
    void Start()
    {
        transform.eulerAngles = new Vector3(Random.Range(0, 255), Random.Range(0, 255), Random.Range(0, 255));
    }
    void Update()
    {
        transform.Rotate(Vector3.down + Vector3.left, rotationRate * Time.deltaTime);
    }
}
