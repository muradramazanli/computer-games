﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public string NextScene;
    // Start is called before the first frame update
    
    public void StartButton()
    {
        SceneManager.LoadScene(NextScene);
    }

    public void QuitButton()
    {
        Application.Quit();
    }
}
