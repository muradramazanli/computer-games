﻿//-------------------------
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;
//-------------------------
public class LevelTimer : MonoBehaviour
{
    //Maximum time to complete level (in secs)
    public float MaxTime = 60f;
    public Text CoinLeftText, TimeLeftText, WinLooseText;
    public GameObject WinLose, RestartButton, QuitButton;
    bool gameOver = false;
    //Countdown
    [SerializeField]
    private float CountDown = 0;
    //-------------------------
    // Use this for initialization
    void Start()
    {
        CountDown = MaxTime;

        WinLose.SetActive(false);
        RestartButton.SetActive(false);
        QuitButton.SetActive(false);
    }
    //-------------------------
    // Update is called once per frame
    void Update()
    {
        if (gameOver)
        {
            return;
        }
        CoinLeftText.text = String.Format("Coin Left: {0}", Coin.CoinCount);
        TimeLeftText.text = String.Format("Time Left: {0}", Mathf.Round(CountDown));

        //Reduce time
        CountDown -= Time.deltaTime;
        //Restart level if time runs out
        if (CountDown <= 0)
        {
            FinishGame(false);
        }
        if (Coin.CoinCount <= 0)
        {
            FinishGame(true);
        }
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void Quit()
    {
        Application.Quit();
    }

    void FinishGame(bool hasWon)
    {
        gameOver = true;
        RestartButton.SetActive(true);
        QuitButton.SetActive(true);
        WinLose.SetActive(true);

        WinLooseText.text = hasWon ? "You are the winner" : "You have lost";

        if (hasWon)
        {
            GameObject[] FireworkSystems = GameObject.FindGameObjectsWithTag("FireWorks");
            foreach (GameObject GO in FireworkSystems)
            {
                GO.GetComponent<ParticleSystem>().Play();
            }

        }


    }


}