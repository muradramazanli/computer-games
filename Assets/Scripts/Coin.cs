﻿using UnityEngine;
using System.Collections;
public class Coin : MonoBehaviour
{
    public static int CoinCount = 0;
    // Use this for initialization
    void Start()
    {
        //Object created, increment coin count
        ++Coin.CoinCount;
    }

    void OnTriggerEnter(Collider Col)
    {
        //If player collected coin, then destroy object
        if (Col.CompareTag("Player"))
            Destroy(gameObject);
    }

    void OnDestroy()
    {
        --Coin.CoinCount;
        
    }
}