﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Blink : MonoBehaviour
{
    public Text BlinkText;
    Vector4 C;
    float speed = 2f;
    // Start is called before the first frame update
    void Start()
    {
        C = BlinkText.color;
    }

    // Update is called once per frame
    void Update()
    {
        BlinkText.color = new Vector4(C[0], C[1], C[2], Mathf.Round(Mathf.PingPong(Time.time * speed, 1)));
    }
}
